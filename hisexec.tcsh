#!/bin/tcsh

if ("$1" == "") then
	echo "at least one argument is required. Use history instead."
	exit 1
endif
set hs = "history | grep $argv:q | cut -f3 - | uniq"
eval $hs | nl
set chosen=$<
if ( "$chosen" == "" ) then
	set chosen = 1
endif
set cmd = "`eval $hs`"
echo "executing: $cmd[$chosen]"
eval $cmd[$chosen]
