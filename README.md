hisexec
==

hisexec is a small tcsh script based on
- grep for searching history
- cut to select everything after the 3rd field
- uniq to filter duplicates
- nl to generate linenumbers
and choose which command to execute by selection the corresponding number.
